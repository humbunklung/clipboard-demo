#include <afx.h>
#include <iostream>

using namespace std;


int main()
{
	CString fromClipboad;
	TCHAR szFormat[MAX_PATH] = {};
	UINT clipboardFmtCnt(0);
	if (OpenClipboard(NULL))
	{
		UINT clipboardFmt(0);
		clipboardFmtCnt = CountClipboardFormats();
		while (clipboardFmt = EnumClipboardFormats(clipboardFmt)) 
		{
			GetClipboardFormatName(clipboardFmt, szFormat, MAX_PATH);
			cout << szFormat << endl;
		}
		HANDLE hData = GetClipboardData(CF_TEXT);
		fromClipboad = static_cast<TCHAR*>(GlobalLock(hData));
		GlobalUnlock(hData);
		CloseClipboard();
	}
	cout << fromClipboad << endl;
	cout << clipboardFmtCnt << endl;
	system("pause");
	return 0;
}
